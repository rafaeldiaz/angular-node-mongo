export class FeedDto {
    _id?: string;
    story_title: string;
    author: string;
    url: string;
    title: string;
    comment_text: string;
    created_at?: string;

}