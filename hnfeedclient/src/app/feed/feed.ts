import { StoryTitle } from '../story-title/storyTitle';
import { StoryUrl } from '../story-url/storyUrl';
import { Author } from '../author/author';
import { CommentText } from '../comment-text/commentText';
import { Title } from '../title/title';
import { Url } from '../url/url';

export class Feed {
    _id?: string;
    created_at: string;
    title: string;
    url: string;
    author: string;
    points: string;
    story_text: string;
    comment_text: string;
    num_comments: string;
    story_id: number;
    story_title: string;
    story_url: string;
    parent_id: number;
    created_at_i: number;
    _tags: any[] = new Array;
    objectID: string;
    _highlightResult: Object = {
        author: Author,
        comment_text: CommentText,
        story_title: StoryTitle,
        story_url: StoryUrl,
        title: Title,
        url: Url
    }

}


export class Hits {
    hits: Feed[] = new Array;
}


