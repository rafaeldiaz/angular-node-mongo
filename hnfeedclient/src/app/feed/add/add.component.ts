import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { FeedService } from '../feed.service';
import { FeedDto } from '../feedDto';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit {

  ngOnInit(
  ) { }

  public feedForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private feedService: FeedService,
    private navCtr: NavController
  ) {
    const url = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    const http = new RegExp("^(http|https)://", "i");
    this.createForm(url, http);
  }

  private createForm(url: string, http: RegExp) {
    this.feedForm = this.formBuilder.group({
      story_title: ['', [Validators.minLength(5), Validators.required]],
      author: ['', [Validators.minLength(3), Validators.required]],
      url: ['', [Validators.pattern(url), Validators.pattern(http), Validators.required]],
      title: [''],
      comment_text: [''],
    });
  }

  createFeed() {
    this.feedService.Create(this.feedForm.value).subscribe(x => {
      this.feedService.newFeed.next(x);
      this.goToBack(x);
    });;
  }


  private goToBack(x: FeedDto) {
    if (x) {
      this.navCtr.navigateBack('/');
      this.feedService.NotifyFull({ time: 3000, message: "New feed created successfully", color: "success" });
    } else {
      this.feedService.NotifyFull({ time: 3000, message: "An error occurred while creating the feed.", color: "danger" });
    }
  }
}
