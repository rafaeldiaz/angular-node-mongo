import { Component, Input, OnInit, ɵbypassSanitizationTrustResourceUrl, ɵbypassSanitizationTrustUrl } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-view-feed-url',
  templateUrl: './view-feed-url.component.html',
  styleUrls: ['./view-feed-url.component.scss'],
})
export class ViewFeedUrlComponent implements OnInit {
  @Input('url') url;
  public ViewURL;
  constructor(
    private modalController:ModalController,
    private dom:DomSanitizer,

  ) { }
  ngOnInit() {
    if (!this.url){this.close()};
    this.ViewURL =  this.dom.bypassSecurityTrustResourceUrl(this.url);
  }

  /**
   * close
   */
  public close() {
    this.modalController.dismiss()
  }
}
