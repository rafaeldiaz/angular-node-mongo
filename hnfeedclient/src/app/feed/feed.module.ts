import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpEventType, HttpResponse, HttpParams } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';

import { FeedPageRoutingModule } from './feed-routing.module';

import { FeedPage } from './feed.page';
import { AddComponent } from './add/add.component';
import { PutComponent } from './put/put.component';
import { ViewComponent } from './view/view.component';
import { FormatdatePipe } from '../formatdate.pipe';
import { DatePipe } from '@angular/common';
import { FeedService } from './feed.service';
import { ViewFeedUrlComponent } from './view-feed-url/view-feed-url.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FeedPage, AddComponent, PutComponent, ViewComponent, FormatdatePipe, ViewFeedUrlComponent],
  exports: [],
  providers: [
    DatePipe,
    FeedService
  ],
})
export class FeedPageModule { }
