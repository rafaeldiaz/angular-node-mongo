import { Injectable } from '@angular/core';
import { Feed, Hits } from './feed';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from, Subject, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';
import { FeedDto } from './feedDto';





@Injectable({
  providedIn: 'root'
})
export class FeedService {

  public base = "http://localhost";
  public port = "3000";
  public route = "feed";
  public newFeed =new Subject<FeedDto>(); 

  constructor(
    private http: HttpClient,
    private toast: ToastController,
  ) { }

  public fetchData() : Observable<boolean>  {
    return this.http.get<any>(this.base + ':' + this.port + '/populate');
  }

  public fetchStaticData() : Observable<boolean>  {
    return this.http.get<any>(this.base + ':' + this.port + '/populate/data');
  }

  public GetOne(id: String): Observable<Feed> {
    return this.http.get<Feed>(this.base + ':' + this.port + '/' + this.route + '/' + id);
  }

  public GetAll(): Observable<Feed[]> {

    return this.http.get<Feed[]>(this.base + ':' + this.port + '/' + this.route);

  }

 
  public GetAllByQuery(query: String): Observable<Hits> {
    return this.http.get<any>('https://hn.algolia.com/api/v1/search_by_date?query=' + query)
      .pipe(
        tap(_ => this.log('fetched Feedes', "success", true)),
        catchError(this.handleError<Hits[]>('Get Feed Error', []))
      );
  }

  public Create(obj: FeedDto): Observable<FeedDto> {
    console.log(obj);

    return this.http.post<any>(this.base + ':' + this.port + '/' + this.route, obj);
  }

  public Put(id: String): Observable<Feed> {
    return this.http.get<any>(this.base + ':' + this.port + '/' + this.route + '/' + id);
  }

  public Delete(id: String): Observable<Feed> {
    return this.http.delete<any>(this.base + ':' + this.port + '/' + this.route + '/' + id);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`, "danger", true);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string, type: string, show: boolean) {

    if (type == "success") {
      if (show == true) {
        this.NotifyFull({ time: 3000, message: `Message: ${message}`, color: "success" });
      }

    }
    if (type == "danger") {
      if (show == true) {
        this.NotifyFull({ time: 3000, message: `Message: ${message}`, color: "danger" });
      }
      this.callLogService(message);
    }



  }

  private callLogService(message: string) {
    console.log(message);
    // This should be send message for log error api
  }

  public NotifyFull(data: {
    time?: number, message: string,
    color?: 'success' | 'warning' | 'danger' | 'dark' | 'primary' | 'secondary' | "normal",
    position?: 'top' | 'bottom' | 'middle'
  }) {
    this.toast.create({
      message: data.message,
      position: data.position,
      duration: data.time ? data.time : 2000,
      color: data.color,
    }).then(v => v.present());
  }



}
