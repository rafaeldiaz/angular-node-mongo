import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, ActionSheetController } from '@ionic/angular';
import { Feed } from './feed';
import { FeedService } from './feed.service';
import { FormatdatePipe } from '../formatdate.pipe';
import { ViewFeedUrlComponent } from './view-feed-url/view-feed-url.component';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {

  public feeds: Feed[] = new Array;

  public fdate = FormatdatePipe;
  constructor(
    public feedService: FeedService,
    public navCr: NavController,
    private modalController: ModalController,
    public actionSheetController:  ActionSheetController
  ) { }

  ngOnInit() {
 
    this.feedService.GetAll().subscribe(f => {
      console.log(f);
      this.feeds = f;
    });

    //Connect to the real api
    // this.feedService.GetAllByQuery("nodejs").subscribe(f => {
    //   console.log(f);
    //   this.feeds = f.hits;
    // });

    this.feedService.newFeed.subscribe(x=>{
      let feed = new Feed;
      feed.author = x.author;
      feed.story_title = x.story_title;
      feed.title = x.title;
      feed.url = x.url;
      feed.comment_text = x.comment_text;
      feed._id = x._id;
      feed.created_at = x.created_at;
      this.feeds.unshift(feed);
    });
  }

  createFeed() {
    this.navCr.navigateForward("add");
  }

  openInNewTab(item: Feed) {
    let url = item.story_url ? item.story_url : item.url;
    var win = window.open(url, '_blank');
    // win.focus();
  }

  fetchData(){

    // Get data from exposed api and populate internal database
    
    this.feedService.fetchData().subscribe(fetch=>{
      if(fetch){
        this.feedService.GetAll().subscribe(f => {
          console.log(f);
          this.feeds = f;
        });

      }else{
        // IF GET data from api fails fill data with static data on server
        // for demonstration purposes only

        this.feedService.fetchStaticData().subscribe(data=>{
          this.feedService.GetAll().subscribe(f => {
            console.log(f);
            this.feeds = f;
          });
        });
        
      }
    });
  }


  async deleteFeed(id: string) {

    const actionSheet = await this.actionSheetController.create({
      header: 'Are you sure to delete this item?',
      buttons: [
        {
          text: 'Delete', role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.deleteRowView(id);
            this.deleteDB(id);
          }
        },
        {
          text: 'Cancel', icon: 'close', role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }

  private deleteRowView(id: string) {
    this.feeds = this.feeds.filter(v => !(v._id == id));

  }

  private deleteDB(id: string) {
    this.feedService.Delete(id).subscribe(v => { });
  }


  /**
   * viewFeed
   */
  public viewFeed(url) {
    this.modalController.create({ component: ViewFeedUrlComponent, componentProps: { url: url } }).then(v => {
      v.present();
    })
  }


}
