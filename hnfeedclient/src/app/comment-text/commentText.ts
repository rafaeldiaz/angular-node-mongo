export class CommentText {
    value: String;
    matchLevel: String;
    fullyHighlighted: Boolean
}