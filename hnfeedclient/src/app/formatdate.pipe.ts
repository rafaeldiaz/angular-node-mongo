import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
@Pipe({
  name: 'formatdate'
})
export class FormatdatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe) { }

  transform(date): unknown {
    let z = this.getDate(date);

    switch (z) {
      case "T": {
        return this.datePipe.transform(date, 'h:mm a');
        break;
      }
      case "Y": {
        return "Yesterday"
        break;
      }
      case "L": {
        return this.datePipe.transform(date, "MMM d");
        break;
      }
      default: {
        //statements; 
        break;
      }
    }



  }

  getDate(date) {
    var a = new Date(date);  // Create_at
    var c = new Date();      // Now

    c.setHours(0);
    c.setMinutes(0);
    c.setSeconds(0, 0);
    
    let d4 = c.getTime() - 1;

    let d3  = d4 - (60*60*24*1000);

    let x = a.getTime();

    if (x > d4)
      return "T";
      else if(( x > d3) &&  (x < d4))
      return "Y";
    else 
      return "L";
  }

}
