export class Url {
    value: String;
    matchLevel: String;
    fullyHighlighted: Boolean;
    matchedWords: any[] = new Array;
}