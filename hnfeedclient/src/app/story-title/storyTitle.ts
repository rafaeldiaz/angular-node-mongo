export class StoryTitle {
    value: String;
    matchLevel: String;
    fullyHighlighted: Boolean;
    matchedWords: any[] = new Array;
}