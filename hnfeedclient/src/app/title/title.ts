export class Title {
    value: String;
    matchLevel: String;
    fullyHighlighted: Boolean;
    matchedWords: any[] = new Array;
}