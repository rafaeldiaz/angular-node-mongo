export class StoryUrl {
    value: String;
    matchLevel: String;
    fullyHighlighted: Boolean;
    matchedWords: any[] = new Array;
}