FROM node:14.15.4

# Dir WORKDIR
WORKDIR /usr/src/app

# Copy Package data
COPY hnfeedserver/package.json ./
RUN yarn --only=development
COPY ./hnfeedserver/ .

# Build App
RUN npm run build

# Run App
ENTRYPOINT [ "npm", "run", "start:prod" ]
