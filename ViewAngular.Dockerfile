FROM node:14.15.4

# Prepare Server
RUN npm install -g angular-http-server

# Dir WORKDIR
WORKDIR /usr/src/app

# Copy Package data
COPY hnfeedclient/package.json ./
RUN yarn

# Move Files
COPY ./hnfeedclient/ .

# Build App
RUN npm run build --prod
WORKDIR  /usr/src/app/www/


# Run App
ENTRYPOINT [ "angular-http-server", "-p", "8088" ]
