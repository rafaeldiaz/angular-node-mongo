# Description

![HNFEED](https://gitlab.com/rafaeldiaz/angular-node-mongo/-/raw/caeac5c53a489efc17b45f6575d0cdcbb30339d3/hnfeedclient/src/assets/img/readme.png)
This is a test with angular, ionic, nestjs, mongo

# Instalation

- Clone this repository

# Run with Docker

This will download docker image, angular cli, nest, mongo. Next will compile and run client and server. Please be patient

## Requirements

1. Docker
2. Docker Compose

## Build App

- Open Console
- Run `docker-compose build`

## Run App

- Run `docker-compose up`
- Open [https://localhost:8088](https://localhost:8088)

## Populate database


For the first time, you can get the exposed API data to populate the internal database data by pressing the red refresh button in the upper right corner of the view.

OR you can

- Open [https://localhost:3000/populate](https://localhost:3000/populate) for repeat the process

# Run separately (Optional)

- Clone this repository

## Requirements

1. Npm
2. Node
3. MongoDB
4. npm i -g @nestjs/cli
5. npm install -g @angular/cli
6. npm install -g @ionic/cli

## Install and run MongoDB

## Run Server

- Open "hnfeedserver" folder in command line
- npm install
- nest start dev
- Open [http://localhost:3000](http://localhost:3000)

## Populate database

For the first time, you can get the exposed API data to populate the internal database data by pressing the red refresh button in the upper right corner of the view.

OR you can

- Open [https://localhost:3000/populate](https://localhost:3000/populate) for repeat the process

## Build Client

- Open "hnfeedclient" folder in command line
- npm install
- ionic serve
- Open [http://localhost:8100](http://localhost:8100)

# Other

Because this is done in ionic, this can run on android, ios, desktop (electron) following the standard steps to compile on these platforms.
