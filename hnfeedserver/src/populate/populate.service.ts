import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Feed } from '../feed/interfaces/Feed';
import { hits} from './data'



@Injectable()
export class PopulateService {

    public data = hits;

    constructor(@InjectModel("Feed") public feedModel: Model<Feed>, private httpService: HttpService) { }
    /**
     * getPopulateFeedRest
     */
    public getPopulateFeedRest() {
        return new Promise<boolean>((like, reject) => {
            // Call Api Data
            this.getRestData(like, reject);
        })

    }

    private getRestData(like: (value: boolean | PromiseLike<boolean>) => void, reject: (reason?: any) => void) {
        this.httpService.get(`https://hn.algolia.com/api/v1/search_by_date?query=nodejs`).subscribe(vals => {
            // Insert data in mongoDB
            this.insertData(vals, like, reject);
        }, (err) => {
            reject(err); // Error get data
        });
    }

    private insertData(vals, like: (value: boolean | PromiseLike<boolean>) => void, reject: (reason?: any) => void) {
        this.feedModel.insertMany(vals.data.hits).then(function (x) {
            like(true); // success insert
            // Error in insert data
        }).catch(function (err) {
            reject(err); // Error on insert data
        });
    }

    async populateFeed() {
        await this.feedModel.insertMany(this.data).then(function (x) {
            console.log("Data inserted")  // Success 

        }).catch(function (error) {
            console.log(error);

        });

    }

}


