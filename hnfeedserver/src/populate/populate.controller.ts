import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';
import { PopulateService } from './populate.service';


@Controller('populate')
export class PopulateController {

    constructor(
        private populateService: PopulateService
    ) {

    }

    //populate from api
    @Get()
    GetPopulation() {
        return new Promise((resolve, reject) => {
            this.createRestPopulate(resolve);
        })
    }

     //populate from data
    @Get('data')
    SetPopulation() {
        return this.populateService.populateFeed();
    }


    private createRestPopulate(resolve: (value: unknown) => void) {
        this.populateService.getPopulateFeedRest().then(ok => {
            resolve(true); // Success Created
        }).catch(err => {
            resolve(false); // Error on created
        });
    }
}
