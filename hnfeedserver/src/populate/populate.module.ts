import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FeedSchema } from 'src/feed/schemas/feed.schema';
import { PopulateController } from './populate.controller';
import { PopulateService } from './populate.service';

@Module({
  imports: [
  MongooseModule.forFeature([{ name: "Feed", schema: FeedSchema }]),
  // HttpModule, For request
  HttpModule.register({
    timeout: 5000,
    maxRedirects: 5,
  }),
],
  controllers: [PopulateController],
  providers: [PopulateService]
})
export class PopulateModule { }
