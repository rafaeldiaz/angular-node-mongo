import { Document } from 'mongoose';
import { Url } from 'url';
import { Author } from './author';
import { CommentText } from './commentText';
import { StoryTitle } from './storyTitle';
import { StoryUrl } from './storyUrl';
import { Title } from './title';

export interface Feed extends Document {
    id?: string;
    created_at?: string;
    title?: string;
    url?: string;
    author?: string;
    points?: number;
    story_text?: string;
    comment_text?: string;
    num_comments?: number;
    story_id?: number;
    story_title?: string;
    story_url?: string;
    parent_id?: number;
    created_at_i?: number;
    _tags?: any[];
    objectID?: string;
    _highlightResult?: {
        author?: Author,
        comment_text?: CommentText,
        story_title?: StoryTitle,
        story_url?: StoryUrl,
        url?: Url,
        title?: Title

    }

}