export class CommentText {
    value: string;
    matchLevel: string;
    fullyHighlighted?: boolean;
    matchedWords: any[] = new Array;
}