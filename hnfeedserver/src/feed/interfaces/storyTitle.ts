export class StoryTitle {
    value: string;
    matchLevel: string;
    fullyHighlighted?: boolean;
    matchedWords: any[] = new Array;
}