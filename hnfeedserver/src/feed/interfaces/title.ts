export class Title {
    value: string;
    matchLevel: string;
    fullyHighlighted?: boolean;
    matchedWords: any[] = new Array;
}