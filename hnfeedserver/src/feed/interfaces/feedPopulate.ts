export interface FeedPopulate {
    id?: string;
    created_at: string;
    title: string;
    url: string;
    author: string;
    points: number;
    story_text: string;
    comment_text: string;
    num_comments: number;
    story_id: number;
    story_title: string;
    story_url: string;
    parent_id: number;
    created_at_i: number;
    _tags: any[];
    objectID: string;
    _highlightResult: {
        title?: {
            value: String,
            matchLevel: String,
            fullyHighlighted?: boolean,
            matchedWords: any[];
        },
        author: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: boolean,
            matchedWords: any[];
        },
        comment_text?: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: Boolean;
            matchedWords: any[];
        },
        url?: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: Boolean;
            matchedWords: any[];
        }
        story_title?: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: boolean,
            matchedWords: any[];
        },
        story_url?: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: boolean,
            matchedWords: any[];
        },
        story_text?: {
            value: String;
            matchLevel: String;
            fullyHighlighted?: Boolean;
            matchedWords: any[];
        }

    }
}