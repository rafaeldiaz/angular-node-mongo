export class StoryUrl {
    value: string;
    matchLevel: string;
    fullyHighlighted?: boolean;
    matchedWords: any[] = new Array;
}