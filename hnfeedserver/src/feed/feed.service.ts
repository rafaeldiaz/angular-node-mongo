import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed } from './interfaces/Feed';
import { FeedDto } from './dto/feedDto';


@Injectable()
export class FeedService {

    constructor(
        @InjectModel("Feed") private feedModel: Model<Feed>
    ) { }

    async getFeed(id: string): Promise<Feed> {
        return this.feedModel.findById(id)
    }

    async getFeeds(): Promise<Feed[]> {

        return this.feedModel.find({}).sort({created_at: 'desc'}).exec();
        //return this.feedModel.find({}).exec();
    }

    async deleteFeed(id: string): Promise<Feed> {
        return this.feedModel.deleteOne({_id:id});
    }

    async createFeed(feedDto: FeedDto): Promise<Feed> {
        const createdFeed = new this.feedModel(feedDto);
        return createdFeed.save();
    }

}
