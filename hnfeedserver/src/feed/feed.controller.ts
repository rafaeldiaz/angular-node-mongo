import { Controller, Delete, Get, Post, Put, Body, Param } from '@nestjs/common';
import { FeedDto } from './dto/feedDto';
import { FeedService } from './feed.service';
import { Feed } from './interfaces/Feed';


@Controller('feed')
export class FeedController {

    constructor(
        private feedService: FeedService
    ) {

    }

    @Get()
    GetFeeds(): Promise<Feed[]> {
        return this.feedService.getFeeds();
    }

    @Get(':id')
    GetFeed(@Param('id') id): Promise<Feed> {
        return this.feedService.getFeed(id);
    }

    @Post()
    CreateFeed(@Body() feed: FeedDto): Promise<Feed> {
        return this.feedService.createFeed(feed);
    }

    @Delete(':id')
    DeleteFeed(@Param('id') id) {
        return this.feedService.deleteFeed(id);
    }

    @Put()
    PutFeed(@Body() feed: Feed): Feed {
        return feed;
    }
}
