import { Schema } from 'mongoose';
import { AuthorSchema } from './author.schema';
import { StoryTitleSchema } from './story_title.schema';
import { StoryUrlSchema } from './story_url.schema';
import { CommentTextSchema } from './commet_text.schema';

export const FeedSchema = new Schema({
    id: String,
    created_at: { type: Date, default: Date.now },
    title: String,
    url: String,
    author: String,
    points: Number,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: Number,
    _tags: Array,
    objectID: String,
    _highlightResult: {
        author: AuthorSchema,
        comment_text: CommentTextSchema,
        story_title: StoryTitleSchema,
        story_url: StoryUrlSchema,

    }

})