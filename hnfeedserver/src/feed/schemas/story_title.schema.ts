import { Schema } from 'mongoose';


export const StoryTitleSchema = new Schema({
    value: String,
    matchLevel: String,
    matchedWords: Array
})