import { Schema } from 'mongoose';


export const StoryUrlSchema = new Schema({
    value: String,
    matchLevel: String,
    matchedWords: Array
})