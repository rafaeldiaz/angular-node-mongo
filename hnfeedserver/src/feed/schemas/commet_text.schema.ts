import { Schema } from 'mongoose';


export const CommentTextSchema = new Schema({
    value: String,
    matchLevel: String,
    fullyHighlighted: Boolean
})