import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed } from './interfaces/Feed';
import { FeedPopulate } from './interfaces/feedPopulate';



@Injectable()
export class FeedPopulateService {
    hits = [
        {
            "created_at": "2021-01-31T05:50:57.000Z",
            "title": null,
            "url": null,
            "author": "tareqak",
            "points": null,
            "story_text": null,
            "comment_text": "I honestly want to have a map of that overlays homes and apartments with Internet connection speed and main Internet nodes\u0026#x2F;routers\u0026#x2F;datacenters. If remote work is a big part of the future, then prioritizing optimal Internet connection parameters as a part of a moving decision seems rational.",
            "num_comments": null,
            "story_id": 25972846,
            "story_title": "A network analysis on cloud gaming: Stadia, GeForce Now and PSNow",
            "story_url": "https://arxiv.org/abs/2012.06774",
            "parent_id": 25973473,
            "created_at_i": 1612072257,
            "_tags": [
                "comment",
                "author_tareqak",
                "story_25972846"
            ],
            "objectID": "25976417",
            "_highlightResult": {
                "author": {
                    "value": "tareqak",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "I honestly want to have a map of that overlays homes and apartments with Internet connection speed and main Internet \u003cem\u003enodes\u003c/em\u003e/routers/datacenters. If remote work is a big part of the future, then prioritizing optimal Internet connection parameters as a part of a moving decision seems rational.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "A network analysis on cloud gaming: Stadia, GeForce Now and PSNow",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://arxiv.org/abs/2012.06774",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
            }
        },
        {
            "created_at": "2021-01-31T01:56:59.000Z",
            "title": null,
            "url": null,
            "author": "hpen",
            "points": null,
            "story_text": null,
            "comment_text": "Hey all, come help turn my project into the best open source Trello alternative with a twist.\u003cp\u003eTech stack is React, NodeJS, MongoDB, and Docker.",
            "num_comments": null,
            "story_id": 25975019,
            "story_title": "Kanception.io Now Open Source",
            "story_url": "https://github.com/hpennington/kanception",
            "parent_id": 25975019,
            "created_at_i": 1612058219,
            "_tags": [
                "comment",
                "author_hpen",
                "story_25975019"
            ],
            "objectID": "25975020",
            "_highlightResult": {
                "author": {
                    "value": "hpen",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "Hey all, come help turn my project into the best open source Trello alternative with a twist.\u003cp\u003eTech stack is React, \u003cem\u003eNodeJS\u003c/em\u003e, MongoDB, and Docker.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Kanception.io Now Open Source",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://github.com/hpennington/kanception",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        }, {
            "created_at": "2021-01-31T01:11:52.000Z",
            "title": null,
            "url": null,
            "author": "gmuslera",
            "points": null,
            "story_text": null,
            "comment_text": "Suppose that there is no need to have in the equation living beings. So you have your von Neumann probes assimilating all the matter in their path into new von Neumann probes, lets call them Borg von Neumann. And they are nodes of a collective \u0026#x2F;distributed \u0026quot;computing\u0026quot; scheme called Alien Civilization 3.0.\u003cp\u003eStill you need time to stop, make your conversion, pick next target, accelerate and deaccelerate. And you may be turning everything into nodes, i.e. suns, exoplanets, etc. As we find stars pretty far away and exoplanets in far away solar systems, we are safe for some million years at the very least, even with relativistic speeds.\u003cp\u003eAnd still, it is about what we think with the wrong mindset and a lot of unknown unknowns to properly understand the problem. Expanding may be obviously wrong for advanced enough... something.",
            "num_comments": null,
            "story_id": 25972111,
            "story_title": "Once we can see them, it’s too late",
            "story_url": "https://www.scottaaronson.com/blog/?p=5253",
            "parent_id": 25974016,
            "created_at_i": 1612055512,
            "_tags": [
                "comment",
                "author_gmuslera",
                "story_25972111"
            ],
            "objectID": "25974739",
            "_highlightResult": {
                "author": {
                    "value": "gmuslera",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "Suppose that there is no need to have in the equation living beings. So you have your von Neumann probes assimilating all the matter in their path into new von Neumann probes, lets call them Borg von Neumann. And they are \u003cem\u003enodes\u003c/em\u003e of a collective /distributed \u0026quot;computing\u0026quot; scheme called Alien Civilization 3.0.\u003cp\u003eStill you need time to stop, make your conversion, pick next target, accelerate and deaccelerate. And you may be turning everything into \u003cem\u003enodes\u003c/em\u003e, i.e. suns, exoplanets, etc. As we find stars pretty far away and exoplanets in far away solar systems, we are safe for some million years at the very least, even with relativistic speeds.\u003cp\u003eAnd still, it is about what we think with the wrong mindset and a lot of unknown unknowns to properly understand the problem. Expanding may be obviously wrong for advanced enough... something.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Once we can see them, it’s too late",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://www.scottaaronson.com/blog/?p=5253",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        }, {
            "created_at": "2021-01-30T22:06:18.000Z",
            "title": null,
            "url": null,
            "author": "tanelpoder",
            "points": null,
            "story_text": null,
            "comment_text": "Mine goes only up to 2 NUMA nodes (as shown in numactl --hardware), despite setting NPS4 in BIOS. I guess it\u0026#x27;s because I have only 2 x 8-core chiplets enabled (?)",
            "num_comments": null,
            "story_id": 25956670,
            "story_title": "Achieving 11M IOPS and 66 GB/S IO on a Single ThreadRipper Workstation",
            "story_url": "https://tanelpoder.com/posts/11m-iops-with-10-ssds-on-amd-threadripper-pro-workstation/",
            "parent_id": 25964871,
            "created_at_i": 1612044378,
            "_tags": [
                "comment",
                "author_tanelpoder",
                "story_25956670"
            ],
            "objectID": "25973471",
            "_highlightResult": {
                "author": {
                    "value": "tanelpoder",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "Mine goes only up to 2 NUMA \u003cem\u003enodes\u003c/em\u003e (as shown in numactl --hardware), despite setting NPS4 in BIOS. I guess it's because I have only 2 x 8-core chiplets enabled (?)",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": ["nodejs"]
                },
                "story_title": {
                    "value": "Achieving 11M IOPS and 66 GB/S IO on a Single ThreadRipper Workstation",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://tanelpoder.com/posts/11m-iops-with-10-ssds-on-amd-threadripper-pro-workstation/",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },

        {
            "created_at": "2021-01-30T20:01:25.000Z",
            "title": null,
            "url": null,
            "author": "nodesocket",
            "points": null,
            "story_text": null,
            "comment_text": "Most certainly businesses as a response will be forced to raise prices and thus increase inflation and make everyday products more expensive.\u003cp\u003eThese studies are obviously politically and progressively motivated (coming from firms such as american progress) so we have to assume a large scale national increase of minimum wage as significant as this would have unintended negative consequences.",
            "num_comments": null,
            "story_id": 25971227,
            "story_title": "Universal Basic Income Is Superior to a $15 Minimum Wage (2019)",
            "story_url": "https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/",
            "parent_id": 25972315,
            "created_at_i": 1612036885,
            "_tags": [
                "comment",
                "author_nodesocket",
                "story_25971227"
            ],
            "objectID": "25972432",
            "_highlightResult": {
                "author": {
                    "value": "\u003cem\u003enodes\u003c/em\u003eocket",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "comment_text": {
                    "value": "Most certainly businesses as a response will be forced to raise prices and thus increase inflation and make everyday products more expensive.\u003cp\u003eThese studies are obviously politically and progressively motivated (coming from firms such as american progress) so we have to assume a large scale national increase of minimum wage as significant as this would have unintended negative consequences.",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_title": {
                    "value": "Universal Basic Income Is Superior to a $15 Minimum Wage (2019)",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-30T19:40:51.000Z",
            "title": null,
            "url": null,
            "author": "nodesocket",
            "points": null,
            "story_text": null,
            "comment_text": "Or how about the big elephant in the room that nobody seems to be bring up; the effect on small businesses that can’t afford to pay employees $15 or $20\u0026#x2F;hr. So, instead of helping the poor, these policies raise unemployment and actually are a detriment to the overall health of the economy.",
            "num_comments": null,
            "story_id": 25971227,
            "story_title": "Universal Basic Income Is Superior to a $15 Minimum Wage (2019)",
            "story_url": "https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/",
            "parent_id": 25972112,
            "created_at_i": 1612035651,
            "_tags": [
                "comment",
                "author_nodesocket",
                "story_25971227"
            ],
            "objectID": "25972238",
            "_highlightResult": {
                "author": {
                    "value": "\u003cem\u003enodes\u003c/em\u003eocket",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "comment_text": {
                    "value": "Or how about the big elephant in the room that nobody seems to be bring up; the effect on small businesses that can’t afford to pay employees $15 or $20/hr. So, instead of helping the poor, these policies raise unemployment and actually are a detriment to the overall health of the economy.",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_title": {
                    "value": "Universal Basic Income Is Superior to a $15 Minimum Wage (2019)",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-30T17:56:30.000Z",
            "title": null,
            "url": null,
            "author": "christiansakai",
            "points": null,
            "story_text": null,
            "comment_text": "Just wondering, I don\u0026#x27;t care much about cryptocurrency, but I do realize that we need cryptocurrency (as a side effect of blockchain) to make the blockchain secure (making it an incentive to run blockchain nodes).\u003cp\u003eAssuming that indeed we as humanity need blockchain (and decentralization technology) in the future, can a blockchain or any other decentralization technology work without cryptocurrency (or any other incentive for that matter)?\u003cp\u003eAny thoughts?",
            "num_comments": null,
            "story_id": 25970101,
            "story_title": "India proposes law to ban cryptocurrencies, create official digital currency",
            "story_url": "https://www.thehindu.com/sci-tech/technology/india-proposes-law-to-ban-cryptocurrencies-create-official-digital-currency/article33703822.ece",
            "parent_id": 25970101,
            "created_at_i": 1612029390,
            "_tags": [
                "comment",
                "author_christiansakai",
                "story_25970101"
            ],
            "objectID": "25971229",
            "_highlightResult": {
                "author": {
                    "value": "christiansakai",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "Just wondering, I don't care much about cryptocurrency, but I do realize that we need cryptocurrency (as a side effect of blockchain) to make the blockchain secure (making it an incentive to run blockchain \u003cem\u003enodes\u003c/em\u003e).\u003cp\u003eAssuming that indeed we as humanity need blockchain (and decentralization technology) in the future, can a blockchain or any other decentralization technology work without cryptocurrency (or any other incentive for that matter)?\u003cp\u003eAny thoughts?",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "India proposes law to ban cryptocurrencies, create official digital currency",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://www.thehindu.com/sci-tech/technology/india-proposes-law-to-ban-cryptocurrencies-create-official-digital-currency/article33703822.ece",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        }, {
            "created_at": "2021-01-30T14:51:09.000Z",
            "title": null,
            "url": null,
            "author": "robbyt",
            "points": null,
            "story_text": null,
            "comment_text": "I manage literally 100k+ Linux nodes for my day job. I don\u0026#x27;t want to manage any more than I need to, so my workstation is a Mac.",
            "num_comments": null,
            "story_id": 25967594,
            "story_title": "macOS Big Sur breaks PostgreSQL because of new security API",
            "story_url": "https://github.com/PostgresApp/PostgresApp/issues/610",
            "parent_id": 25968793,
            "created_at_i": 1612018269,
            "_tags": [
                "comment",
                "author_robbyt",
                "story_25967594"
            ],
            "objectID": "25969512",
            "_highlightResult": {
                "author": {
                    "value": "robbyt",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "I manage literally 100k+ Linux \u003cem\u003enodes\u003c/em\u003e for my day job. I don't want to manage any more than I need to, so my workstation is a Mac.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "macOS Big Sur breaks PostgreSQL because of new security API",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://github.com/PostgresApp/PostgresApp/issues/610",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-30T13:26:05.000Z",
            "title": null,
            "url": null,
            "author": "2Gkashmiri",
            "points": null,
            "story_text": null,
            "comment_text": "twitter becoming activitypub compliant would be a welcome step for that community. why? because it would give instant credibility to the federated network of thousands of nodes who can now talk to twitter as well.\u003cp\u003edoesnt matter if twitter becomes another \u0026quot;instance\u0026quot; because technically it would be.\u003cp\u003ei for one am running a bunch of \u0026quot;off grid networks\u0026quot; which are currently only in my home network because thats what i care about but i am thinking of opening up to the internet as a private network.\u003cp\u003ethis would mean i can allow a twitter user to communicate with my private network without them having to switch over to my network entirely or create yet another account. sure twitter can ban all outside connections, make this decentralized only in name but what benefit would that give? as far as i know, twitter is looking to avoid the responsibility of moderating its users by \u0026quot;federating\u0026quot;, i think give reddit like mod roles to users for a specific community? or something else?\u003cp\u003eright now if they do go ahead with activitypub, they can safely say \u0026quot;hey, here are our Terms. if you break them you are out, otherwise you can join any of the twitter \u0026quot;instances\u0026quot; managed by these people with their own terms.",
            "num_comments": null,
            "story_id": 25960225,
            "story_title": "An overview of the decentralized social ecosystem [pdf]",
            "story_url": "https://matrix.org/_matrix/media/r0/download/twitter.modular.im/981b258141aa0b197804127cd2f7d298757bad20",
            "parent_id": 25960225,
            "created_at_i": 1612013165,
            "_tags": [
                "comment",
                "author_2Gkashmiri",
                "story_25960225"
            ],
            "objectID": "25968856",
            "_highlightResult": {
                "author": {
                    "value": "2Gkashmiri",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "twitter becoming activitypub compliant would be a welcome step for that community. why? because it would give instant credibility to the federated network of thousands of \u003cem\u003enodes\u003c/em\u003e who can now talk to twitter as well.\u003cp\u003edoesnt matter if twitter becomes another \u0026quot;instance\u0026quot; because technically it would be.\u003cp\u003ei for one am running a bunch of \u0026quot;off grid networks\u0026quot; which are currently only in my home network because thats what i care about but i am thinking of opening up to the internet as a private network.\u003cp\u003ethis would mean i can allow a twitter user to communicate with my private network without them having to switch over to my network entirely or create yet another account. sure twitter can ban all outside connections, make this decentralized only in name but what benefit would that give? as far as i know, twitter is looking to avoid the responsibility of moderating its users by \u0026quot;federating\u0026quot;, i think give reddit like mod roles to users for a specific community? or something else?\u003cp\u003eright now if they do go ahead with activitypub, they can safely say \u0026quot;hey, here are our Terms. if you break them you are out, otherwise you can join any of the twitter \u0026quot;instances\u0026quot; managed by these people with their own terms.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "An overview of the decentralized social ecosystem [pdf]",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://matrix.org/_matrix/media/r0/download/twitter.modular.im/981b258141aa0b197804127cd2f7d298757bad20",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-30T09:31:16.000Z",
            "title": null,
            "url": null,
            "author": "hamburglar",
            "points": null,
            "story_text": null,
            "comment_text": "It is utterly bizarre to me that not only are the answers almost universally “no, I couldn’t” but also, “no, and it’a totally ridiculous to expect anyone to be able to.”\u003cp\u003eI make myself do it occasionally just to make sure I’m not getting too lazy about paying attention to details. I can build a basic web site in both golang and nodejs, from scratch, using a plain editor with no autocomplete. It’s actually a very good feeling. I can even do a react app from scratch if we aren’t counting webpack config.\u003cp\u003eMy last proof of concept project at work, the server side was running behind so I spit out an extremely basic but totally functional node server and frontend in an afternoon without using any fancy frameworks and the Java dude was still trying to build the same thing three days after the demo was done. It’s pretty useful to just be able to write it straight from your keyboard without having to go googling a bunch of stuff.\u003cp\u003eWhy is everyone acting like it’s impossible or unreasonable?",
            "num_comments": null,
            "story_id": 25961420,
            "story_title": "Can you make a basic web app without Googling? I can’t",
            "story_url": "https://web.eecs.utk.edu/~azh/blog/webappwithoutgoogling.html",
            "parent_id": 25961420,
            "created_at_i": 1611999076,
            "_tags": [
                "comment",
                "author_hamburglar",
                "story_25961420"
            ],
            "objectID": "25967657",
            "_highlightResult": {
                "author": {
                    "value": "hamburglar",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "It is utterly bizarre to me that not only are the answers almost universally “no, I couldn’t” but also, “no, and it’a totally ridiculous to expect anyone to be able to.”\u003cp\u003eI make myself do it occasionally just to make sure I’m not getting too lazy about paying attention to details. I can build a basic web site in both golang and \u003cem\u003enodejs\u003c/em\u003e, from scratch, using a plain editor with no autocomplete. It’s actually a very good feeling. I can even do a react app from scratch if we aren’t counting webpack config.\u003cp\u003eMy last proof of concept project at work, the server side was running behind so I spit out an extremely basic but totally functional node server and frontend in an afternoon without using any fancy frameworks and the Java dude was still trying to build the same thing three days after the demo was done. It’s pretty useful to just be able to write it straight from your keyboard without having to go googling a bunch of stuff.\u003cp\u003eWhy is everyone acting like it’s impossible or unreasonable?",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Can you make a basic web app without Googling? I can’t",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://web.eecs.utk.edu/~azh/blog/webappwithoutgoogling.html",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-30T08:04:58.000Z",
            "title": "Ask HN: Is there an Etherpad Lite “even lighter” version?",
            "url": null,
            "author": "josephernest",
            "points": 1,
            "story_text": "Requirements: no binary needed, no NodeJs needed, no client\u0026#x2F;server database.\u003cp\u003eJust PHP and SQLite.\u003cp\u003eNumber of writers: just a team of 3 people\u003cp\u003eCollaborative real time plain text editing, less than 1 MB of text written per month.",
            "comment_text": null,
            "num_comments": 1,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1611993898,
            "_tags": [
                "story",
                "author_josephernest",
                "story_25967243",
                "ask_hn"
            ],
            "objectID": "25967243",
            "_highlightResult": {
                "title": {
                    "value": "Ask HN: Is there an Etherpad Lite “even lighter” version?",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "author": {
                    "value": "josephernest",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_text": {
                    "value": "Requirements: no binary needed, no \u003cem\u003eNodeJs\u003c/em\u003e needed, no client/server database.\u003cp\u003eJust PHP and SQLite.\u003cp\u003eNumber of writers: just a team of 3 people\u003cp\u003eCollaborative real time plain text editing, less than 1 MB of text written per month.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-29T23:02:08.000Z",
            "title": null,
            "url": null,
            "author": "hrjfkgkgk",
            "points": null,
            "story_text": null,
            "comment_text": "they all do, its just that some are better than others. i dont actually like php, but it has plenty of advantages when it comes to quick web app development even without googling.\u003cp\u003ehad a client once where its workers were day dreaming of a job in the valley and ofc they were using python. they bashed php on a daily basis, and as they were doing it, the php api was sole codebase running in production (essentially the company’s product) and running reliably for a few years.\u003cp\u003ethe python workers were still struggling with basics such as queues and isc, and digging through non existent or vaguely written documentation. was a fun experience watching them struggle as i basically cloned repos, read the code or entry points for that code and get things shipped.\u003cp\u003esince then i started loving python. development is slower and my invoices keep coming. love it. occasionally i write some lib thats already been implemented in plenty of php and nodejs packages.\u003cp\u003eoh same client had a ruby codebase that kept failing and couldnt find devs to work on it. fun times.",
            "num_comments": null,
            "story_id": 25961420,
            "story_title": "Can you make a basic web app without googling? I can't",
            "story_url": "https://web.eecs.utk.edu/~azh/blog/webappwithoutgoogling.html",
            "parent_id": 25963923,
            "created_at_i": 1611961328,
            "_tags": [
                "comment",
                "author_hrjfkgkgk",
                "story_25961420"
            ],
            "objectID": "25963975",
            "_highlightResult": {
                "author": {
                    "value": "hrjfkgkgk",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "they all do, its just that some are better than others. i dont actually like php, but it has plenty of advantages when it comes to quick web app development even without googling.\u003cp\u003ehad a client once where its workers were day dreaming of a job in the valley and ofc they were using python. they bashed php on a daily basis, and as they were doing it, the php api was sole codebase running in production (essentially the company’s product) and running reliably for a few years.\u003cp\u003ethe python workers were still struggling with basics such as queues and isc, and digging through non existent or vaguely written documentation. was a fun experience watching them struggle as i basically cloned repos, read the code or entry points for that code and get things shipped.\u003cp\u003esince then i started loving python. development is slower and my invoices keep coming. love it. occasionally i write some lib thats already been implemented in plenty of php and \u003cem\u003enodejs\u003c/em\u003e packages.\u003cp\u003eoh same client had a ruby codebase that kept failing and couldnt find devs to work on it. fun times.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Can you make a basic web app without googling? I can't",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://web.eecs.utk.edu/~azh/blog/webappwithoutgoogling.html",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },

        {
            "created_at": "2021-01-28T16:56:32.000Z",
            "title": "Ask HN: Why DevOps is still complicated in 2021?",
            "url": null,
            "author": "aristofun",
            "points": 10,
            "story_text": "Please point to my dark spot, because I really want to understand something.\u003cp\u003eAren\u0026#x27;t like ~90% of all web infra deployment usecases eventually come down to just \u0026quot;run N instances of my M apps on these L nodes, and make X of them available for incoming traffic (+ optionally create me few overlay networks inside cluster)\u0026quot;?\u003cp\u003eWith lower level details resolved on per container\u0026#x2F;app level.\u003cp\u003eWhy then k8s and all those tools make even simplest cases look so complicated?\u003cp\u003eWhy then tools following \u0026quot;simplify things\u0026quot; ideology (like Swarm) are barely surviving in the shadow of monsters like k8s and alike?\u003cp\u003eWhy aren\u0026#x27;t there widely popular tools that compete for simplicity with heroku?\u003cp\u003eDo I miss some big point somewhere or people just really love to play with complicated solutions of \u0026quot;not so complicated\u0026quot; problems?",
            "comment_text": null,
            "num_comments": 10,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1611852992,
            "_tags": [
                "story",
                "author_aristofun",
                "story_25944804",
                "ask_hn"
            ],
            "objectID": "25944804",
            "_highlightResult": {
                "title": {
                    "value": "Ask HN: Why DevOps is still complicated in 2021?",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "author": {
                    "value": "aristofun",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_text": {
                    "value": "Please point to my dark spot, because I really want to understand something.\u003cp\u003eAren't like ~90% of all web infra deployment usecases eventually come down to just \u0026quot;run N instances of my M apps on these L \u003cem\u003enodes\u003c/em\u003e, and make X of them available for incoming traffic (+ optionally create me few overlay networks inside cluster)\u0026quot;?\u003cp\u003eWith lower level details resolved on per container/app level.\u003cp\u003eWhy then k8s and all those tools make even simplest cases look so complicated?\u003cp\u003eWhy then tools following \u0026quot;simplify things\u0026quot; ideology (like Swarm) are barely surviving in the shadow of monsters like k8s and alike?\u003cp\u003eWhy aren't there widely popular tools that compete for simplicity with heroku?\u003cp\u003eDo I miss some big point somewhere or people just really love to play with complicated solutions of \u0026quot;not so complicated\u0026quot; problems?",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-27T19:28:50.000Z",
            "title": null,
            "url": null,
            "author": "joshxyz",
            "points": null,
            "story_text": null,
            "comment_text": "When I was researching stuff I\u0026#x27;ve read a lot of good things about Phoenix, I honestly think socket.io and ws NodeJS libraries are inferior compared to that.\u003cp\u003eIt\u0026#x27;s just almost pure luck that the guy who wrote uWebSockets (written in C \u0026amp; C++) also wrote NodeJS bindings, otherwise we\u0026#x27;ll be stuck with the ones with sub-par api, docs, and performance.",
            "num_comments": null,
            "story_id": 25930177,
            "story_title": "Data fetching on the web still sucks",
            "story_url": "https://performancejs.com/post/hde6a90/Data-Fetching-on-the-Web-Still-Sucks",
            "parent_id": 25932112,
            "created_at_i": 1611775730,
            "_tags": [
                "comment",
                "author_joshxyz",
                "story_25930177"
            ],
            "objectID": "25932714",
            "_highlightResult": {
                "author": {
                    "value": "joshxyz",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "When I was researching stuff I've read a lot of good things about Phoenix, I honestly think socket.io and ws \u003cem\u003eNodeJS\u003c/em\u003e libraries are inferior compared to that.\u003cp\u003eIt's just almost pure luck that the guy who wrote uWebSockets (written in C \u0026amp; C++) also wrote \u003cem\u003eNodeJS\u003c/em\u003e bindings, otherwise we'll be stuck with the ones with sub-par api, docs, and performance.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Data fetching on the web still sucks",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://performancejs.com/post/hde6a90/Data-Fetching-on-the-Web-Still-Sucks",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-27T17:14:48.000Z",
            "title": null,
            "url": null,
            "author": "jangid",
            "points": null,
            "story_text": null,
            "comment_text": "This article talks about one of my pains. This idea of apps was once explored by Mozilla. Remember XUL? XULRunner? I wish they come up with something of that sort soon. Not only that. I also want Mozilla to come up with an alternate for NodeJS. Why can\u0026#x27;t Firefox\u0026#x27;s JS engine be separated?",
            "num_comments": null,
            "story_id": 25927479,
            "story_title": "Firefox just walked away from a key piece of the open web [progressive web apps]",
            "story_url": "https://www.fastcompany.com/90597411/mozilla-firefox-no-ssb-pwa-support",
            "parent_id": 25927479,
            "created_at_i": 1611767688,
            "_tags": [
                "comment",
                "author_jangid",
                "story_25927479"
            ],
            "objectID": "25930776",
            "_highlightResult": {
                "author": {
                    "value": "jangid",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "This article talks about one of my pains. This idea of apps was once explored by Mozilla. Remember XUL? XULRunner? I wish they come up with something of that sort soon. Not only that. I also want Mozilla to come up with an alternate for \u003cem\u003eNodeJS\u003c/em\u003e. Why can't Firefox's JS engine be separated?",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Firefox just walked away from a key piece of the open web [progressive web apps]",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://www.fastcompany.com/90597411/mozilla-firefox-no-ssb-pwa-support",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        },
        {
            "created_at": "2021-01-27T11:48:59.000Z",
            "title": null,
            "url": null,
            "author": "myersmike",
            "points": null,
            "story_text": null,
            "comment_text": "Hire Nodejs developers to get the best Node.JS development services for web and mobile apps. Contact our experienced Nodejs web app development team and get free consultation now.",
            "num_comments": null,
            "story_id": 25927083,
            "story_title": "Node.js Development Company – Node.js Developer Service Provider",
            "story_url": "https://www.aistechnolabs.com/node-js-development/",
            "parent_id": 25927083,
            "created_at_i": 1611748139,
            "_tags": [
                "comment",
                "author_myersmike",
                "story_25927083"
            ],
            "objectID": "25927084",
            "_highlightResult": {
                "author": {
                    "value": "myersmike",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "comment_text": {
                    "value": "Hire \u003cem\u003eNodejs\u003c/em\u003e developers to get the best Node.JS development services for web and mobile apps. Contact our experienced \u003cem\u003eNodejs\u003c/em\u003e web app development team and get free consultation now.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Node.js Development Company – Node.js Developer Service Provider",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                },
                "story_url": {
                    "value": "https://www.aistechnolabs.com/node-js-development/",
                    "matchLevel": "none",
                    "matchedWords": [
                    ]
                }
            }
        }
    ];




    constructor(@InjectModel("Feed") public feedModel: Model<Feed>) { }

    async populateFeed() {
        await this.feedModel.insertMany(this.hits).then(function (x) {
            console.log("Data inserted")  // Success 

        }).catch(function (error) {
            console.log(error);

        });

    }




}

