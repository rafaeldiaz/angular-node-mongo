export class FeedDto {
    story_title: string;
    author: string;
    url: string;
    title: string;
    comment_text: string;
}