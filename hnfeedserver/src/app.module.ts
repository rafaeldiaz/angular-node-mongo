import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeedModule } from './feed/feed.module';
import { MongooseModule } from '@nestjs/mongoose';
import { PopulateModule } from './populate/populate.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: async (config: ConfigService) => {
      const HostDBConfig = config.get('DBHOST');
      const HostDB = HostDBConfig ? HostDBConfig : 'localhost';
      console.log('DBHOST: '+HostDB);
      return {
        uri: `mongodb://${HostDB}/nest`
      }
    },
    inject: [ConfigService],
  }), FeedModule, PopulateModule],
  controllers: [AppController,],
  providers: [AppService],
})
export class AppModule { }
